package com.epam.theatherapp.controller;

import com.epam.theatherapp.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class findUserById extends HttpServlet {

    private static final Logger SLF4JLOG = LoggerFactory.getLogger(FindUserByEmail.class);
    private UserDao userDao = new UserDao();

    public findUserById() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SLF4JLOG.info("Listed all users");
        String email = request.getParameter("email");
        request.setAttribute("users", userDao.getUserByEmail(email));
        RequestDispatcher rd = request.getRequestDispatcher("/finduserbyid.jsp");
        rd.forward(request, response);
    }
}
