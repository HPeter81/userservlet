package com.epam.theatherapp.controller;

import com.epam.theatherapp.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FindUserByFirstName extends HttpServlet {
    private static final Logger SLF4JLOG = LoggerFactory.getLogger(FindUserByFirstName.class);
    private UserDao userDao = new UserDao();

    public FindUserByFirstName() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SLF4JLOG.info("Listed All Users By FirstName");
        String firstName = request.getParameter("firstName");
        request.setAttribute("users", userDao.getUserByFirstName(firstName));
        RequestDispatcher rd = request.getRequestDispatcher("/firstname.jsp");
        rd.forward(request, response);
    }
}
