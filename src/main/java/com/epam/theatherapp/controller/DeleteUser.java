package com.epam.theatherapp.controller;

import com.epam.theatherapp.dao.UserDao;
import com.epam.theatherapp.domain.Role;
import com.epam.theatherapp.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DeleteUser extends HttpServlet {
    private static final Logger SLF4JLOG = LoggerFactory.getLogger(DeleteUser.class);

    private UserDao userDao = new UserDao();

    public DeleteUser() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        boolean errorOccured = false;

        if (request.getParameter("firstName").isEmpty()) {
            request.setAttribute("firstName", "User name is missing");
            SLF4JLOG.warn("User name is missing");
            rd = request.getRequestDispatcher("/deleteuser.jsp");
            errorOccured = true;
        }

        if (!errorOccured) {
            long id = Long.parseLong(request.getParameter("id"));
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String email = request.getParameter("email");
            Role role = null;
            String r = request.getParameter("role");
            if (r.equals(Role.valueOf("ADMIN"))) {
                role = Role.ADMIN;
            }
            else if (r.equals(Role.valueOf("USER"))) {
                role = Role.USER;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String bd = request.getParameter("birthDate");
            Date birthDate = null;
            try {
                birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(bd);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            User user = new User(id, firstName, lastName, email, role, birthDate);
            userDao.remove(user);
            request.setAttribute("infoMessage", "User deleted successfully");
            SLF4JLOG.info("User deleted successfully");
            request.setAttribute("users", userDao.getAll());
            rd = request.getRequestDispatcher("/listuser.jsp");
        } else {
            rd = request.getRequestDispatcher("/error.jsp");
        }
        rd.forward(request, response);
    }
}
