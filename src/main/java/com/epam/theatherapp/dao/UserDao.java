package com.epam.theatherapp.dao;

import com.epam.theatherapp.domain.Role;
import com.epam.theatherapp.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UserDao {

    private static final Logger SLF4JLOG = LoggerFactory.getLogger(UserDao.class);

    private static Map<Long, User> users = new HashMap<>();

    static {
        users.put(1L, new User(1L, "Admin", "Admin", "admin@admin.com", Role.ADMIN, new Date(1970-1-1)));
        users.put(2L, new User(2L, "User", "User", "users@users.com", Role.USER,  new Date(1990-10-10)));
    }


    public User getUserByEmail( String email) {
        for (User user : users.values()) {
            if (email.equals(user.getEmail())) {
                return user;
            }
        }
        return null;
    }

    public User getUserByFirstName(String firstName) {
        for (User user : users.values()) {
            if (firstName.equals(user.getFirstName())) {
                return user;
            }
        }
        return null;
    }

    public void save(User user) {
        Long id = Math.incrementExact(2L);
        user.setId(id);
        users.put(id, user);
    }

    public void remove(User user) {
        users.remove(user.getId());
    }

    public User getById( Long id) {
        return users.get(id);
    }

    public Collection<User> getAll() {
        return users.values();
    }
}
