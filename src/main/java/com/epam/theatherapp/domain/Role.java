package com.epam.theatherapp.domain;

public enum Role {
    ADMIN("Admin"), USER("User");

    public final String role;

    Role(String role) {
        this.role = role;
    }

    public String role() {
        return role;
    }

    public String toString() {
        return this.role;
    }
}
