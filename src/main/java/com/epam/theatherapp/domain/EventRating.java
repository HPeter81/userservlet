package com.epam.theatherapp.domain;

public enum EventRating {

    LOW,

    MID,

    HIGH;
}
