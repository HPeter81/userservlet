<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
</head>

<body>

<div>Save New User</div>

<form action="saveuserresult" method="POST">

    <b>Id:</b>
    <br>
    <input type="long" name="id" required />
    <br>
    <b>First Name:</b>
    <br>
    <input type="text" name="firstName" required />
    <br>
    <b>Last Name:</b>
    <br>
    <input type="text" name="lastName" required />
    <br>
    <b>E-Mail:</b>
    <br>
    <input type="text" name="email" required />
    <br>
    <b>Birth Date:</b>
    <br>
    <input type="date" name="birthDate" required />
    <br>
    <b>Role<b>
    <br>
    <select name="role">
        <option value="admin">Admin</option>
        <option value="user">User</option>
    </select>
    <br>
    <br>
    <input type="submit" value="Save User" />

</body>
</html>