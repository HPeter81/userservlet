<!DOCTYPE.html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
</head>

<body>
    <table border ="1" width="50%">

        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>E-mail</th>
            <th>Booked Tickets</th>
            <th>Birth Date</th>
        </tr>

        <c:forEach items="${user}" var="user">
            <tr>
                <td><c:out value="${user.id}"></td>
                <td><c:out value="${user.firstName}"></td>
                <td><c:out value="${user.lastName}"></td>
                <td><c:out value="${user.email}"></td>
                <td><c:out value="${user.tickets}"></td>
                <td><c:out value="${user.birthDate}"></td>
            </tr>
    </table>
</body>


<div>
<a href="/finduser" class="button">Back to Users List</a>
</div>
</html>