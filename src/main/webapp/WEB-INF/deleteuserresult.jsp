<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>

<body>
    <b>Delete User Result</b>

    <p>
        <c:out value="${result}">
    </p>
    <p>
        <c:out value="${error}">
    </p>

    <div>
    <a href="/listuser" class="button">Back to Users List</a>
    </div>

</body>
</html>