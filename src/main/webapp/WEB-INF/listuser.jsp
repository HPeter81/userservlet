<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
</head>

<body>

    <div>List of Users</div>

    <table border ="1" width="50%">

        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>E-mail</th>
            <th>Birth Date</th>

        </tr>

       <c:forEach items="${users.values()}" var="user">
            <tr>
                <td><c:expr value="${user.id}"></td>
                <td><c:out value="${user.firstname}"></td>
                <td><c:out value="${user.lastname}"></td>
                <td><c:out value="${user.email}"></td>
                <td><c:out value="${user.birthdate}"></td>
            </tr>
    </table>

    <div class="buttonbox">
    <a href="/finduserbyemail" class="button">Find User By E-Mail</a>
    <br>
    <a href="/finduserbyid" class="button">Find User By ID</a>
    <br>
    <a href="/saveuser" class="button">Save New User</a>
    <br>
    <a href="/deleteuser" class="button">Delete User</a>



</body>

</html>